﻿//+------------------------------------------------------------------+
//|                            MetaTrader 5 API Manager risk manager |
//|                   Copyright 2001-2016, MetaQuotes Software Corp. |
//|                                        http://www.metaquotes.net |
//+------------------------------------------------------------------+
namespace RiskManager
{
    using MetaQuotes.MT5CommonAPI;
    using MetaQuotes.MT5ManagerAPI;
    using Npgsql;
    using System.Collections;
    using SharpYaml.Serialization;
    using System.Collections.Generic;
    using System.IO;
    using System;

    public static class Logger
    {
        public static void Init(string PATH)
        {
            var config = new NLog.Config.LoggingConfiguration();

            var FILENAME = Path.GetFullPath(Path.Combine(PATH, "riskmanager.log"));
            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = FILENAME };
            var logconsole = new NLog.Targets.ColoredConsoleTarget("logconsole");

            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logfile);

            NLog.LogManager.Configuration = config;
        }
        
        public static void Info(string msg, params object[] args)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Info(msg, args);
        }

        public static void Error(string msg, params object[] args)
        {
            var logger = NLog.LogManager.GetCurrentClassLogger();
            logger.Error(msg, args);
        }
    }

    public class OpenOrder
    {
        protected bool _isReady;
        protected Hashtable _openData;
        protected Hashtable _closeData;
        protected bool p_added_commission;
        protected bool p_added_open;
        protected bool p_added_close_state;
        protected double p_sl;
        protected double p_tp;
        protected double p_swap;
        protected NpgsqlConnection _conn;
        protected bool p_isOpen;
        public string state;

        public double sl
        {
            get
            {
                return p_sl;
            }

            set
            {
                p_sl = value;
            }
        }

        public double tp
        {
            get
            {
                return p_tp;
            }

            set
            {
                p_tp = value;
            }
        }

        public double swap
        {
            get
            {
                return p_swap;
            }

            set
            {
                p_swap = value;
            }
        }

        public bool isOpen()
        {
            return p_isOpen;
        }

        public void markAsOpen()
        {
            p_isOpen = true;
        }

        public void AddOpenCommission(double commission)
        {
            _openData.Add("commission", commission);
            p_added_commission = true;
        }

        public bool IsOpenReady() {
            return (p_added_commission && p_added_open);
        }

        public bool IsCloseReady()
        {
            return p_added_close_state;
        }

        public OpenOrder(NpgsqlConnection conn, ulong volume)
        {
            _conn = conn;
            _openData = new Hashtable();
            _closeData = new Hashtable();
            p_isOpen = false;
            p_swap = 0;
            state = "open";
        }

        public void AddOpenParams(ulong login, ulong ticket, string type, double volume, string symbol, 
            double price, long tstamp, double sl, double tp, double bid, double ask)
        {
            _openData.Add("login", login.ToString());
            _openData.Add("ticket", ticket.ToString());
            _openData.Add("type", type);
            _openData.Add("volume", volume * 0.0001);
            _openData.Add("symbol", symbol);
            _openData.Add("price", price);

            DateTimeOffset dt = DateTimeOffset.FromUnixTimeSeconds(tstamp);
            _openData.Add("time", dt);

            _openData.Add("sl", sl);
            _openData.Add("tp", tp);
            _openData.Add("bid", bid);
            _openData.Add("ask", ask);

            p_sl = sl;
            p_tp = tp;
            p_added_open = true;
        }

        public bool ExecUpdateQuery(long tstamp, double sl, double tp, ulong ticket_id)
        {
            DateTimeOffset _tstamp = DateTimeOffset.FromUnixTimeSeconds(tstamp);
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = _conn;
                cmd.CommandText = "INSERT INTO trades.updates (" +
                    "tstamp, sl, tp, ticket_id) VALUES (" +
                    "@tstamp, @sl, @tp, @ticket_id)";

                cmd.Parameters.AddWithValue("tstamp", _tstamp);
                cmd.Parameters.AddWithValue("sl", sl);
                cmd.Parameters.AddWithValue("tp", tp);
                cmd.Parameters.AddWithValue("ticket_id", ticket_id.ToString());

                cmd.ExecuteNonQuery();
                return true;
            }
        }

        public bool ExecOpenQuery()
        {
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = _conn;
                cmd.CommandText = "INSERT INTO trades.positions (" +
                    "login, ticket, type, volume, symbol, open_price, open_time, open_sl, open_tp, " +
                    "open_commission, swap, state, open_bid, open_ask) VALUES(" +
                    "@login, @ticket, @type, @volume, @symbol, @open_price, @open_time, @open_sl, @open_tp, " +
                    "@open_commission, @swap, @state, @open_bid, @open_ask)";

                cmd.Parameters.AddWithValue("login", _openData["login"]);
                cmd.Parameters.AddWithValue("ticket", _openData["ticket"]);
                cmd.Parameters.AddWithValue("type", _openData["type"]);
                cmd.Parameters.AddWithValue("volume", _openData["volume"]);
                cmd.Parameters.AddWithValue("symbol", _openData["symbol"]);
                cmd.Parameters.AddWithValue("open_price", _openData["price"]);
                cmd.Parameters.AddWithValue("open_time", _openData["time"]);
                cmd.Parameters.AddWithValue("open_sl", _openData["sl"]);
                cmd.Parameters.AddWithValue("open_tp", _openData["tp"]);
                cmd.Parameters.AddWithValue("open_bid", _openData["bid"]);
                cmd.Parameters.AddWithValue("open_ask", _openData["ask"]);
                cmd.Parameters.AddWithValue("open_commission", _openData["commission"]);
                cmd.Parameters.AddWithValue("swap", 0);
                cmd.Parameters.AddWithValue("state", "open");

                cmd.ExecuteNonQuery();

                return true;
            }   
        }

        public void AddCloseParams(ulong ticket, long tstamp, double volume, double price,
            double commission, double profit, double close_bid, double close_ask,
            double close_sl, double close_tp)
        {
            _closeData["ticket_id"] = ticket.ToString();
            _closeData["tstamp"] = DateTimeOffset.FromUnixTimeSeconds((long)tstamp);
            _closeData["volume"] = volume * 0.0001;
            _closeData["price"] = price;
            _closeData["commission"] = commission;
            _closeData["profit"] = profit;
            _closeData["bid"] = close_bid;
            _closeData["ask"] = close_ask;
            _closeData["sl"] = close_sl;
            _closeData["tp"] = close_tp;
        }

        public bool ExecUpdateSwapQuery(ulong ticket, double new_swap)
        {
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = _conn;
                cmd.CommandText = "UPDATE trades.positions SET " +
                    "swap = @new_swap WHERE ticket = @ticket";
                cmd.Parameters.AddWithValue("new_swap", new_swap);
                cmd.Parameters.AddWithValue("ticket", ticket.ToString());

                cmd.ExecuteNonQuery();

                return true;
            }
        }

        public bool ExecChangeStateQuery(ulong ticket, string new_state)
        {
            if (new_state == "closed")
            {
                p_added_close_state = true;
            }

            // save to database
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = _conn;
                cmd.CommandText = "UPDATE trades.positions SET state = @new_state " +
                            "WHERE ticket = @ticket";
                cmd.Parameters.AddWithValue("new_state", new_state);
                cmd.Parameters.AddWithValue("ticket", ticket.ToString());

                cmd.ExecuteNonQuery();

                return true;
            }
        }

        public bool ExecCloseQuery()
        {
            // save to database 
            using (var cmd = new NpgsqlCommand())
            {
                cmd.Connection = _conn;
                cmd.CommandText = "INSERT INTO trades.partials (" +
                    "close_time, close_volume, close_commission, close_price, profit, close_bid, close_ask," +
                    "close_sl, close_tp, ticket_id) VALUES (" +
                    "@close_time, @close_volume, @close_commission, @close_price, @profit, @close_bid, @close_ask," +
                    "@close_sl, @close_tp, @ticket_id)";
                cmd.Parameters.AddWithValue("close_time", _closeData["tstamp"]);
                cmd.Parameters.AddWithValue("close_volume", _closeData["volume"]);
                cmd.Parameters.AddWithValue("close_commission", _closeData["commission"]);
                cmd.Parameters.AddWithValue("close_price", _closeData["price"]);
                cmd.Parameters.AddWithValue("profit", _closeData["profit"]);
                cmd.Parameters.AddWithValue("close_bid", _closeData["bid"]);
                cmd.Parameters.AddWithValue("close_ask", _closeData["ask"]);
                cmd.Parameters.AddWithValue("close_sl", _closeData["sl"]);
                cmd.Parameters.AddWithValue("close_tp", _closeData["tp"]);
                cmd.Parameters.AddWithValue("ticket_id", _closeData["ticket_id"]);

                cmd.ExecuteNonQuery();

                return true;
            }
        }
    }

    class CDealSink : CIMTDealSink
    {
        CIMTManagerAPI m_manager = null;
        NpgsqlConnection _conn;
        Hashtable _myOrders;

        public MTRetCode Initialize(CIMTManagerAPI manager, ref NpgsqlConnection conn, ref Hashtable myOrders)
        {
            //--- checking
            if (manager == null)
                return (MTRetCode.MT_RET_ERR_PARAMS);
            //--- 
            m_manager = manager;

            //---
            _conn = conn;

            //---      
            _myOrders = myOrders;

            return (RegisterSink());
        }
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public override void OnDealAdd(CIMTDeal deal)
        {
            if (deal != null)
            {
                string str = deal.Print();
                m_manager.LoggerOut(EnMTLogCode.MTLogOK, "Mdeal {0} has been added", str);
                Logger.Info("deal {0} has been added", str);
                ulong ticket = deal.PositionID();
                OpenOrder order;

                if (_myOrders.Contains(ticket))
                {
                    order = (OpenOrder)_myOrders[ticket];
                }
                else
                {
                    order = new OpenOrder(_conn, deal.Volume());
                    _myOrders.Add(ticket, order);
                }

                if (!order.isOpen())
                {
                    order.AddOpenCommission(deal.Commission());

                    Logger.Info("Add comm open: " + deal.Commission());

                    if (order.IsOpenReady())
                    {
                        order.ExecOpenQuery();
                        Logger.Info("Executing open query from DealAdd");
                    }

                    order.markAsOpen();
                }
                else
                {
                    MTTickShort tick;
                    m_manager.TickLast(deal.Symbol(), out tick);

                    ulong vol = deal.Volume();

                    order.AddCloseParams(
                        ticket,
                        deal.Time(),
                        vol,
                        deal.Price(),
                        deal.Commission(),
                        deal.Profit(),
                        tick.bid,
                        tick.ask,
                        deal.PriceSL(),
                        deal.PriceTP()
                    );

                    if (order.ExecCloseQuery())
                    {
                        if (order.IsCloseReady())
                        {
                            _myOrders.Remove(ticket);
                        }
                        else
                        {
                            if (order.state == "open")
                            {
                                order.state = "partial";
                                order.ExecChangeStateQuery(ticket, order.state);
                            }
                        }
                    }
                }
            }
        }
    }

    class CPositionSink : CIMTPositionSink
    {
        CIMTManagerAPI m_manager = null;
        NpgsqlConnection _conn;
        Hashtable _myOrders;

        //+------------------------------------------------------------------+
        //| Init native implementation                                       |
        //+------------------------------------------------------------------+
        public MTRetCode Initialize(CIMTManagerAPI manager, ref NpgsqlConnection conn, ref Hashtable myOrders)
        {
            //--- checking
            if (manager == null)
                return (MTRetCode.MT_RET_ERR_PARAMS);
            //--- 
            m_manager = manager;

            //---
            _conn = conn;

            //---
            _myOrders = myOrders;

            return (RegisterSink());
        }
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public override void OnPositionAdd(CIMTPosition position)
        {
            if (position != null)
            {
                string str = position.Print();
                m_manager.LoggerOut(EnMTLogCode.MTLogOK, "Mpos {0} has been added", str);
                Logger.Info("pos {0} has been added", str);
                
                string type;
                switch (position.Action())
                {
                    case 0:
                        type = "buy";
                        break;
                    case 1:
                        type = "sell";
                        break;
                    default:
                        return;
                }

                ulong ticket = position.Position();
                OpenOrder order;

                if (_myOrders.Contains(ticket))
                {
                    order = (OpenOrder)_myOrders[ticket];
                }
                else
                {
                    order = new OpenOrder(_conn, position.Volume());
                    _myOrders.Add(ticket, order);
                }

                string symbol = position.Symbol();
                MTTickShort tick;
                m_manager.TickLast(symbol, out tick);

                order.AddOpenParams(
                    position.Login(),
                    position.Position(),
                    type,
                    position.Volume(),
                    symbol,
                    position.PriceOpen(),
                    position.TimeCreate(),
                    position.PriceSL(),
                    position.PriceTP(),
                    tick.bid,
                    tick.ask
                );

                Logger.Info("Added {0}", position.Position());

                if (order.IsOpenReady())
                {
                    order.ExecOpenQuery();
                    Logger.Info("Executing open query from PositionAdd");
                }
            }
        }
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public override void OnPositionUpdate(CIMTPosition position)
        {
            if (position != null)
            {
                ulong ticket = position.Position();
                if (_myOrders.Contains(ticket))
                {
                    OpenOrder order = (OpenOrder)_myOrders[ticket];
                    double sl = position.PriceSL();
                    double tp = position.PriceTP();
                    double swap = position.Storage();

                    Logger.Info("Swap: {0}", swap);

                    if (swap != order.swap)
                    {
                        order.ExecUpdateSwapQuery(ticket, swap);
                        order.swap = swap;
                    }

                    if (sl != order.sl || tp != order.tp)
                    {
                        order.ExecUpdateQuery(position.TimeUpdate(), sl, tp, ticket);

                        string str = position.Print();
                        m_manager.LoggerOut(EnMTLogCode.MTLogOK, "Mpos {0} has been updated", str);
                        Logger.Info("pos {0} has been updated", str);
                        Logger.Info("upd SL " + position.PriceSL());
                        Logger.Info("upd TP " + position.PriceTP());

                        order.sl = sl;
                        order.tp = tp;
                    }
                }
            }
        }

        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public override void OnPositionDelete(CIMTPosition position)
        {
            if (position != null)
            {
                ulong ticket = position.Position();
                if (_myOrders.Contains(ticket))
                {
                    OpenOrder order = (OpenOrder)_myOrders[ticket];

                    order.ExecChangeStateQuery(ticket, "closed");

                    string str = position.Print();
                    m_manager.LoggerOut(EnMTLogCode.MTLogOK, "Mpos {0} has been deleted", str);
                    Logger.Info("pos {0} has been deleted", str);
                }
            }
        }
    }
   
 
    //+------------------------------------------------------------------+
    //| Manager                                                          |
    //+------------------------------------------------------------------+
    class CManager : IDisposable
    {
        //--- connect timeout in milliseconds
        uint MT5_CONNECT_TIMEOUT = 30000;
        //---
        CIMTManagerAPI m_manager   =null;
        CIMTDealArray m_deal_array = null;
        CIMTUser m_user = null;
        CIMTAccount m_account = null;

        CPositionSink m_position_sink = null;
        CDealSink m_deal_sink = null;
        
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public CManager()
        {
        }

        ~CManager()
        {
            Logger.Info("Shutting down ...");
        }
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public void Dispose()
        {
            Shutdown();
        }
        //+------------------------------------------------------------------+
        //| Initialize library                                               |
        //+------------------------------------------------------------------+
        public bool Initialize(string API_PATH, ref Hashtable myOrders, ref NpgsqlConnection conn)
        {
            string message = string.Empty;
            MTRetCode res = MTRetCode.MT_RET_OK_NONE;
            //--- loading manager API

            if ((res = SMTManagerAPIFactory.Initialize(API_PATH)) != MTRetCode.MT_RET_OK)
            {
                message = string.Format("Loading manager API failed ({0})", res);
                Logger.Error(message);
                return (false);
            }

            //--- creating manager interface
            m_manager = SMTManagerAPIFactory.CreateManager(SMTManagerAPIFactory.ManagerAPIVersion, out res);
            if ((res != MTRetCode.MT_RET_OK) || (m_manager == null))
            {
                SMTManagerAPIFactory.Shutdown();
                message = string.Format("Creating manager interface failed ({0})", (res == MTRetCode.MT_RET_OK ? "Managed API is null" : res.ToString()));
                Logger.Error(message);
                return (false);
            }

            //--- creating position pump
            m_position_sink = new CPositionSink();
            m_position_sink.RegisterSink();
            m_position_sink.Initialize(m_manager, ref conn, ref myOrders);
            if ((res = m_manager.PositionSubscribe(m_position_sink)) != MTRetCode.MT_RET_OK)
            {
                m_manager.LoggerOut(EnMTLogCode.MTLogErr, "Dealer: creating position sink failed");
                message = "Dealer: creating position sink failed";
                Logger.Error(message);
                return (false);
            }

            //--- creating deal pump
            m_deal_sink = new CDealSink();
            m_deal_sink.RegisterSink();
            m_deal_sink.Initialize(m_manager, ref conn, ref myOrders);
            if ((res = m_manager.DealSubscribe(m_deal_sink)) != MTRetCode.MT_RET_OK)
            {
                m_manager.LoggerOut(EnMTLogCode.MTLogErr, "Dealer: creating position sink failed");
                message = "Dealer: creating deal sink failed";
                Logger.Error(message);
                return (false);
            }

            //--- all right
            return (true);
        }
        //+------------------------------------------------------------------+
        //| Login                                                            |
        //+------------------------------------------------------------------+
        public bool Login(string server, UInt64 login, string password)
        {
            //--- connect
            MTRetCode res = m_manager.Connect(server, login, password, null, 
                CIMTManagerAPI.EnPumpModes.PUMP_MODE_POSITIONS
                , MT5_CONNECT_TIMEOUT);
            if (res != MTRetCode.MT_RET_OK)
            {
                m_manager.LoggerOut(EnMTLogCode.MTLogErr, "Connection failed ({0})", res);
                return (false);
            }
            CIMTConSymbol[] symbols = new CIMTConSymbol[16];
            MTRetCode[] results = new MTRetCode[16];
            for (uint i = 0; i < symbols.Length; i++)
            {
                symbols[i] = m_manager.SymbolCreate();
                m_manager.SymbolNext(i, symbols[i]);
                string desc = symbols[i].Description();
                desc += "!";
                symbols[i].Description(desc);
            }

            return (true);
        }
        //+------------------------------------------------------------------+
        //|                                                                  |
        //+------------------------------------------------------------------+
        public void Logout()
        {
            //--- disconnect manager
            if (m_manager != null)
                m_manager.Disconnect();
        }
        //+------------------------------------------------------------------+
        //| Shutdown                                                         |
        //+------------------------------------------------------------------+
        public void Shutdown()
        {
            if (m_deal_array != null)
            {
                m_deal_array.Dispose();
                m_deal_array = null;
            }
            if (m_position_sink != null)
            {
                m_position_sink.Dispose();
                m_position_sink = null;
            }
            if (m_manager != null)
            {
                m_manager.Dispose();
                m_manager = null;
            }
            if (m_user != null)
            {
                m_user.Dispose();
                m_user = null;
            }
            if (m_account != null)
            {
                m_account.Dispose();
                m_account = null;
            }
            SMTManagerAPIFactory.Shutdown();
        }
        //+------------------------------------------------------------------+
        //| Get array of dealer balance operation                            |
        //+------------------------------------------------------------------+
        public bool GetUserDeal(out CIMTDealArray deals, UInt64 login, DateTime time_from, DateTime time_to)
        {
            deals = null;
            //--- request array
            MTRetCode res = m_manager.DealRequest(login, SMTTime.FromDateTime(time_from), SMTTime.FromDateTime(time_to), m_deal_array);
            if (res != MTRetCode.MT_RET_OK)
            {
                m_manager.LoggerOut(EnMTLogCode.MTLogErr, "DealRequest fail({0})", res);
                return (false);
            }
            //---
            
            deals = m_deal_array;
            return (true);
        }
        //+------------------------------------------------------------------+
        //| Get user info string                                             |
        //+------------------------------------------------------------------+
        public bool GetUserInfo(UInt64 login, out string str)
        {
            str = string.Empty;

            //--- request user from server
            m_user.Clear();
            MTRetCode res = m_manager.UserRequest(login, m_user);
            if (res != MTRetCode.MT_RET_OK)
            {
                m_manager.LoggerOut(EnMTLogCode.MTLogErr, "UserRequest error ({0})", res);
                return (false);
            }
            //--- format string
            str = string.Format("{0},{1},{2},1:{3}", m_user.Name(), m_user.Login(), m_user.Group(), m_user.Leverage());
            //---
            return (true);
        }

        //+------------------------------------------------------------------+
        //| Get user info string                                             |
        //+------------------------------------------------------------------+
        public bool GetAccountInfo(UInt64 login, out string str)
        {
            str = "";
            return (true);
        }
        //+------------------------------------------------------------------+
        //| Dealer operation                                                 |
        //+------------------------------------------------------------------+
        public bool DealerBalance(UInt64 login, double amount, uint type, string comment, bool deposit)
        {
            return (true);
        }
    }

    class Program
    {
        public class Config
        {
            public Dictionary<string, string> mt5 { get; set; }
            public Dictionary<string, string> psql { get; set; }
        }

        public static void Main()
        {
            string DESKTOP_PATH = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string API_PATH = Path.GetFullPath(Path.Combine(DESKTOP_PATH, @"API\"));
            string CONFIG_PATH = Path.GetFullPath(Path.Combine(DESKTOP_PATH, @"API\config.yml"));

            //--- redirect stdout to file
            string STDOUT_PATH = Path.GetFullPath(Path.Combine(API_PATH, @"stderr.log"));
            FileStream ostrm = new FileStream(STDOUT_PATH, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(ostrm);
            writer.AutoFlush = true;
            //Console.SetOut(writer);
            Console.SetError(writer);

            //--- set up yaml parser
            var input = new StreamReader(CONFIG_PATH);
            var deserializer = new Serializer();
            var config = (Config)deserializer.Deserialize(input, typeof(Config));

            ulong login = Convert.ToUInt64(config.mt5["login"]);
            string pass = config.mt5["pass"];
            string server = config.mt5["server"];

            //--- set up logger 
            Logger.Init(API_PATH);

            string db_username = config.psql["user"];
            string db_pass = config.psql["pass"];
            string db_host = config.psql["host"];

            //--- set up postgresql 
            var connString = $"Host={db_host};Username={db_username};Password={db_pass};Database=virtualrisk";
            NpgsqlConnection conn = new NpgsqlConnection(connString);
            conn.Open();

            CManager manager = new CManager();

            Hashtable myOrders = new Hashtable();

            if (manager.Initialize(API_PATH, ref myOrders, ref conn))
            {
                if (manager.Login(server, login, pass))
                {
                    Logger.Info("Login Successful !!!");
                    while (true)
                    {
                        System.Threading.Thread.Sleep(100);
                    }
                }
                else
                {
                    Logger.Error("Failed to login with (server, login, pass)");
                }
            }
            else
            {
                Logger.Error("Failed to initialize");
            }

            conn.Close();
        }
    }
}
//+------------------------------------------------------------------+
