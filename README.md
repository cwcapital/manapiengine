This project was designed designed to run on Windows Server 2016 or higher.

# Installation  
- Download and install the newest version of GIt for windows. Executable can be found on git-scm.com website.

- Download and install Postgresql 11.2. Executable can be found on. During installation process, when prompted, install Stack Builder. Under Categories -> Database Drivers, select pgJDBC v42.2.2-1

- Download and install .NET Framework 4.7.2

# Setup
To set up the initial database and schema, open windows command prompt:
``` bat
cd path\to\project\dir
set PATH=%PATH%;C:\Program Files\PostgreSQL\11\bin
set PGPASSWORD=POSTGRES_PASSWORD_HERE
set PGCLIENTENCODING=utf-8
psql -U postgres -v cwcappass=CWCAP_PASSWD_HERE -f setup.sql
```

# Deployment
Copy the MT5 API to the Desktop.

The contents of the directory follow:
```
Desktop
 | API
   | MetaQuotes.MT5CommonAPI64.dll
   | MetaQuotes.MT5ManagerAPI64.dll
   | MT5APIManager.dll
   | MT5APIManager.h
   | MT5APIManager64.dll 
```

If developing on Visual Studio, publish the project solution and zip it. 

Inside of the API folder the same directory, create a new file, 
config.yml in the following format:

~\Desktop\API\config.yml
``` yaml
mt5:
  login: MY_LOGIN
  pass: MY_PASS
  server: MY_SERVER

psql:
  user: MY_USER
  pass: MY_PASS
  host: MY_HOST
```

**Make sure port 5432 is open**
