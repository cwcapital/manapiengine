﻿DROP DATABASE IF EXISTS virtualrisk;

CREATE DATABASE virtualrisk;

\connect virtualrisk;

DROP USER IF EXISTS cwcap;
CREATE USER cwcap WITH PASSWORD '$(cwcappass)';
GRANT ALL PRIVILEGES ON DATABASE virtualrisk to cwcap;
CREATE SCHEMA trades;

CREATE TABLE trades.positions(
LOGIN		VARCHAR(12)		NOT NULL,
TICKET		VARCHAR(12)		NOT NULL,
TYPE		VARCHAR(5)		NOT NULL,
VOLUME		DECIMAL(8,2)		NOT NULL,
SYMBOL		VARCHAR(12)		NOT NULL,
OPEN_PRICE	DECIMAL(15,8)		NOT NULL,
OPEN_TIME	TIMESTAMP		NOT NULL,
OPEN_SL		DECIMAL(15,8)	NOT NULL,
OPEN_TP		DECIMAL(15,8)	NOT NULL,
OPEN_BID	DECIMAL(15,8)	NOT NULL,
OPEN_ASK	DECIMAL(15,8)	NOT NULL,
OPEN_COMMISSION	DECIMAL(15,8)	NOT NULL,
SWAP		DECIMAL(15,8)	NOT NULL,
STATE		VARCHAR(12)		NOT NULL,
PRIMARY KEY (TICKET)
);

CREATE TABLE trades.updates(
  TSTAMP			TIMESTAMP		NOT NULL,
  SL				DECIMAL(15,8)	NOT NULL,
  TP				DECIMAL(15,8)	NOT NULL,
  TICKET_ID	VARCHAR(12) REFERENCES trades.positions(TICKET)
);

CREATE TABLE trades.partials(
   CLOSE_TIME		TIMESTAMP		NOT NULL,
   CLOSE_VOLUME		DECIMAL(8,2)		NOT NULL,
   CLOSE_COMMISSION	DECIMAL(15,8)	NOT NULL,
   CLOSE_PRICE		DECIMAL(15,8)	NOT NULL,
   PROFIT		DECIMAL(15,8)	NOT NULL,
   CLOSE_BID		DECIMAL(15,8)	NOT NULL,
   CLOSE_ASK		DECIMAL(15,8)	NOT NULL,
   CLOSE_SL		DECIMAL(15,8)	NOT NULL,
   CLOSE_TP		DECIMAL(15,8)	NOT NULL,
   TICKET_ID	VARCHAR(12)	REFERENCES trades.positions(TICKET)
);
